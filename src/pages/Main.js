import React, { useState } from 'react'

import Projet from '../components/projets/Projet'
import Logo from '../assets/img/logo/logo.svg'
import { Link } from 'react-router-dom'
import Fetch from '../hooks/Fetch'

/* `const projets` is an array of objects that contains information about different projects. Each
object has four properties: `id`, `nom`, `lien`, and `img`. `id` is a unique identifier for each
project, `nom` is the name of the project, `lien` is the URL link to the project, and `img` is the
image associated with the project. This array is used to display a list of projects on the main page
of the website. */
const projets = [
    { id: 1, nom: 'Projet 1', lien: 'https://exemple.com/projet1', img: Logo },
    { id: 2, nom: 'Projet 2', lien: 'https://exemple.com/projet2', img: Logo },
    { id: 3, nom: 'Projet 3', lien: 'https://exemple.com/projet2', img: Logo },
]

function Main() {
    const [count, setCount] = useState(100)
    return (
        <div className='container text-center'>
            <Fetch />
            <h1>Liste de mes projets</h1>

            <p>Count: {count}</p>
            <button onClick={() => setCount(count + 123)} className='btn btn-primary'>
                Increment
            </button>

            <br />
            <br />

            <div className='row'>
                {projets.map((data) => (
                    <>
                        <div className='col'>
                            <Link to={`/projects/${data.id}`} key={data.id} title={data.nom}>
                                <Projet nom={data.nom} img={data.img} lien={data.lien} />
                            </Link>
                        </div>
                    </>
                ))}
            </div>
        </div>
    )
}

export default Main
