import { render, screen } from '@testing-library/react'
import App from './App'

/* This is a test case for a React component called `App`. It checks whether the component renders a
link with the text "learn react". */
test('renders learn react link', () => {
    render(<App />)
    const linkElement = screen.getByText(/learn react/i)
    expect(linkElement).toBeInTheDocument()
})
