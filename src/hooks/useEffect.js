import React, { useState, useEffect } from 'react'

/**
 * The `Effect` function uses the `useState` and `useEffect` hooks to create a timer that updates the
 * `time` state variable every second and displays the elapsed time in a div element.
 * @returns The component is returning a div element that displays the elapsed time in seconds using
 * the `time` state variable. The text "Temps écoulé : " is also included in the div element.
 */
export default function Effect() {
    /* `const [time, setTime] = useState(0)` is using the `useState` hook to declare a state variable
called `time` and a function called `setTime` to update its value. The initial value of `time` is
set to 0. */
    const [time, setTime] = useState(0)

    /* `useEffect` is a hook that allows us to perform side effects in functional components. In this
    case, it is setting up a timer using `setInterval` that updates the `time` state variable every
    second. The `return` statement inside `useEffect` is a cleanup function that clears the timer
    using `clearInterval` when the component unmounts. The empty array `[]` passed as the second
    argument to `useEffect` ensures that the effect is only run once when the component mounts. */
    useEffect(() => {
        const timer = setInterval(() => {
            setTime((prevTime) => prevTime + 1)
        }, 1000)

        return () => {
            clearInterval(timer)
        }
    }, [])

    return <div>Temps écoulé : {time} secondes</div>
}
