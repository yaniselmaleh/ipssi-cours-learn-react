import React, { useState } from 'react'

const Dark = () => {
    const [modalOpen, setModalOpen] = useState(false)
    const [darkMode, setDarkMode] = useState(false)

    const openModal = () => {
        setModalOpen(true)
    }

    const closeModal = () => {
        setModalOpen(false)
    }

    const toggleDarkMode = () => {
        setDarkMode(!darkMode)
    }

    return (
        <div className={darkMode ? 'dark' : ''}>
            <h1>Page</h1>

            <button onClick={openModal} className='btn btn-primary'>
                En savoir plus ?
            </button>

            {modalOpen && (
                <div className='modal'>
                    <div className='modal-content'>
                        <h2>Je suis une modal</h2>
                        <p>Bla Bla Bla</p>
                        <button onClick={closeModal} className='btn btn-primary'>
                            Ferme moi
                        </button>
                    </div>
                </div>
            )}

            <button onClick={toggleDarkMode} className='btn btn-dark-mode'>
                {darkMode ? 'Light Mode' : 'Dark Mode'}
            </button>

            <style>
                {`
                    .modal {
                        position: fixed;
                        top: 0;
                        left: 0;
                        width: 100%;
                        height: 100%;
                        background-color: rgba(0, 0, 0, 0.5);
                        display: flex;
                        justify-content: center;
                        align-items: center;
                    }

                    .modal-content {
                        width: 800px;
                        background-color: white;
                        padding: 20px;
                        border-radius: 4px;
                        box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
                    }

                    .dark {
                        background-color: black;
                        color: white;
                    }
                    `}
            </style>
        </div>
    )
}

export default Dark
