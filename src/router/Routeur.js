import React from 'react'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Main from '../pages/Main'
import About from '../pages/About'
import ProjetDetailsPage from '../components/projets/ProductDetailsPage'
import NoMatch from '../router/NoMatch'
import Layout from '../layout/layout'
import Logo from '../assets/img/logo/logo.svg'

const Routeur = () => {
    const data = [
        { id: 1, nom: 'Projet 1', url: 'https://exemple.com/projet1', img: Logo, yan: 'COUCOU1' },
        { id: 2, nom: 'Projet 2', url: 'https://exemple.com/projet2', img: Logo, yan: 'COUCOU2' },
    ]

    return (
        <BrowserRouter>
            <Layout>
                <Routes>
                    <Route path='/' element={<Main />} />
                    <Route path='/about' element={<About />} />
                    <Route path='/projects/:id' element={<ProjetDetailsPage projets={data} />} />
                    <Route path='*' element={<NoMatch />} />
                </Routes>
            </Layout>
        </BrowserRouter>
    )
}

export default Routeur
