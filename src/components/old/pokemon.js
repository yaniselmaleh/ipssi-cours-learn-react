const PokemonComponent = () => {
    const data = [
        {
            id: 1,
            nom: 'Bulbasaur',
            type: ['Grass', 'Poison'],
            caracteristiques: {
                'P.V': 45,
                'Attaque': 49,
                'Defense': 49,
                'Att. Spé.': 65,
                'Déf. Spé.': 65,
                'Vitesse': 45,
            },
        },
        {
            id: 2,
            nom: 'Ivysaur',
            type: ['Grass', 'Poison'],
            caracteristiques: {
                'P.V': 60,
                'Attaque': 62,
                'Defense': 63,
                'Att. Spé.': 80,
                'Déf. Spé.': 80,
                'Vitesse': 60,
            },
        },
    ]

    return (
        <div>
            <ul>
                {data.map((pokemon) => (
                    <li key={pokemon.id}>
                        <h2>{pokemon.nom}</h2>
                        <p>Type: {pokemon.type.join(', ')}</p>
                        <p>Caractéristiques:</p>
                        <ul>
                            {Object.entries(pokemon.caracteristiques).map(([key, value]) => (
                                <li key={key}>
                                    {key}: {value}
                                </li>
                            ))}
                        </ul>
                    </li>
                ))}
            </ul>
        </div>
    )
}

export default PokemonComponent
