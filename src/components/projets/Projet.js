/**
 * The Projet function returns a card component with an image, title, and button that redirects to a
 * specified link.
 * @returns The `Projet` component is being returned, which renders a card with an image, project name,
 * and a button that redirects to the project link.
 */
const Projet = ({ nom, img, lien }) => {
    return (
        <>
            <div className='card' style={{ border: '1px solid black' }}>
                <img
                    src={img}
                    alt={nom}
                    style={{ width: '200px', height: 'auto' }}
                    className='card-img-top'
                />
                <div className='card-body'>
                    <h2 className='card-title'>{nom}</h2>
                    <a
                        href={lien}
                        title={nom}
                        target='_blank'
                        rel='noreferrer'
                        className='btn btn-primary'
                    >
                        Redirection vers le projet
                    </a>
                </div>
            </div>
        </>
    )
}

export default Projet
