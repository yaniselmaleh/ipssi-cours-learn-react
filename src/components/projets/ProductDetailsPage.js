import { useParams } from 'react-router-dom'

/**
 * This is a React component that displays details of a project based on its ID, including its name,
 * image, and URL.
 * @returns The `ProjetDetailsPage` component is being returned.
 */
const ProjetDetailsPage = ({ projets }) => {
    const { id } = useParams()

    // Recherche du projet correspondant à l'ID dans la liste des projets
    const projet = projets.find((projet) => projet.id.toString() === id)

    if (!projet) {
        // Gérer le cas où le projet n'est pas trouvé
        return <h2>Projet non trouvé</h2>
    }

    return (
        <>
            <h2>Nom du projet {projet.nom}</h2>
            <p>
                Image :{' '}
                <img src={projet.img} alt={projet.nom} style={{ width: '100px', height: 'auto' }} />
            </p>
            <a href={projet.url} title={projet.nom} target='_blank' rel='noreferrer'>
                Lien
            </a>
            {/* Afficher d'autres données du projet */}
        </>
    )
}

export default ProjetDetailsPage
