import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import reportWebVitals from './reportWebVitals'

/* This code is creating a root React component using `ReactDOM.createRoot()` and rendering the `<App
/>` component inside it using `root.render()`. The `createRoot()` method is a new feature in React
18 that allows for asynchronous rendering and improved performance. The `<React.StrictMode>`
component is used to highlight potential problems in the code during development. */
const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
    <React.StrictMode>
        <App />
    </React.StrictMode>,
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
